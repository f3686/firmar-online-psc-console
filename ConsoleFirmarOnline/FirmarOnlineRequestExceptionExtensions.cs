﻿using FirmarOnline.Model;

namespace Api
{
    /// <summary>
    /// Métodos de extensión para objeto <see cref="FirmarOnlineRequestException"/>
    /// </summary>
    public static class FirmarOnlineRequestExceptionExtensions
    {
        public static void WriteConsole(this FirmarOnlineRequestException e)
        {
            // Salida con error
            Console.WriteLine("Response KO!!!!!!");
            // Muestra de contenido de excepción por consola
            Console.WriteLine("Excepción:");
            Console.WriteLine("\tStatusCode: " + e.StatusCode);
            Console.WriteLine("\tMessage: " + e.Message);
            Console.WriteLine("\tReasonPhrase: " + e.ReasonPhrase);

            if (e.ProblemDetails != null)
            {
                Console.WriteLine("\tProblem Details:");
                Console.WriteLine("\t\tType: " + e.ProblemDetails.Type);
                Console.WriteLine("\t\tTitle: " + e.ProblemDetails.Title);
                Console.WriteLine("\t\tStatusCode: " + e.ProblemDetails.Status);
                Console.WriteLine("\t\tDetail: " + e.ProblemDetails.Detail);
                Console.WriteLine("\t\tInstance: " + e.ProblemDetails.Instance);
                if (e.ProblemDetails.Errors != null && e.ProblemDetails.Errors.Count > 0)
                {
                    Console.WriteLine("\t\tErrors:");
                    foreach (var error in e.ProblemDetails.Errors)
                    {
                        Console.WriteLine("\t\t\t{0} : ", error.Key);
                        foreach (string param in error.Value)
                        {
                            Console.WriteLine("\t\t\t\t" + param);
                        }
                    }
                }
            }
        }
    }
}
