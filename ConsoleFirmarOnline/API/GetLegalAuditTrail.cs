﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class GetLegalAuditTrail
    {
        /// <summary>
        /// Obtención de Legal Audit Trail (Json firmado(información del sobre + evidencias))
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obtención Legal Audit Trail de un sobre...");

                // Llamada a API de FirmarOnline
                var stream = await client.GetLegalAuditTrailAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Stream de documento obtenido. Longitud: " + stream.Length);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
