﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using System.Text.Json;

namespace Api
{
    public class GetAuditTrail
    {
        /// <summary>
        /// Obtención de las trazas de eventos generados en el procesamiento de un sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obtención de las trazas de eventos de un sobre...");

                // Llamada a API de FirmarOnline
                var collectionAuditEvent = await client.GetAuditTrailAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                // Formateo de salida a Json
                var jsonOptions = new JsonSerializerOptions { WriteIndented = true };
                Console.WriteLine(JsonSerializer.Serialize(collectionAuditEvent, jsonOptions));
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
