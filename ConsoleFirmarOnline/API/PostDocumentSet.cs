﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using FirmarOnline.Model.PSC;

namespace Api
{
    public class PostDocumentSet
    {
        // Ejemplo de documento pdf en base 64
        private static readonly string pdfB64 = "JVBERi0xLjMNCiXi48/TDQoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL091dGxpbmVzIDIgMCBSDQovUGFnZXMgMyAwIFINCj4+DQplbmRvYmoNCg0KMiAwIG9iag0KPDwNCi9UeXBlIC9PdXRsaW5lcw0KL0NvdW50IDANCj4+DQplbmRvYmoNCg0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0NvdW50IDINCi9LaWRzIFsgNCAwIFIgNiAwIFIgXSANCj4+DQplbmRvYmoNCg0KNCAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDw8DQovRm9udCA8PA0KL0YxIDkgMCBSIA0KPj4NCi9Qcm9jU2V0IDggMCBSDQo+Pg0KL01lZGlhQm94IFswIDAgNjEyLjAwMDAgNzkyLjAwMDBdDQovQ29udGVudHMgNSAwIFINCj4+DQplbmRvYmoNCg0KNSAwIG9iag0KPDwgL0xlbmd0aCAxMDc0ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBBIFNpbXBsZSBQREYgRmlsZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIFRoaXMgaXMgYSBzbWFsbCBkZW1vbnN0cmF0aW9uIC5wZGYgZmlsZSAtICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjY0LjcwNDAgVGQNCigganVzdCBmb3IgdXNlIGluIHRoZSBWaXJ0dWFsIE1lY2hhbmljcyB0dXRvcmlhbHMuIE1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NTIuNzUyMCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDYyOC44NDgwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjE2Ljg5NjAgVGQNCiggdGV4dC4gQW5kIG1vcmUgdGV4dC4gQm9yaW5nLCB6enp6ei4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjA0Ljk0NDAgVGQNCiggbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDU5Mi45OTIwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNTY5LjA4ODAgVGQNCiggQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA1NTcuMTM2MCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBFdmVuIG1vcmUuIENvbnRpbnVlZCBvbiBwYWdlIDIgLi4uKSBUag0KRVQNCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCjYgMCBvYmoNCjw8DQovVHlwZSAvUGFnZQ0KL1BhcmVudCAzIDAgUg0KL1Jlc291cmNlcyA8PA0KL0ZvbnQgPDwNCi9GMSA5IDAgUiANCj4+DQovUHJvY1NldCA4IDAgUg0KPj4NCi9NZWRpYUJveCBbMCAwIDYxMi4wMDAwIDc5Mi4wMDAwXQ0KL0NvbnRlbnRzIDcgMCBSDQo+Pg0KZW5kb2JqDQoNCjcgMCBvYmoNCjw8IC9MZW5ndGggNjc2ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBTaW1wbGUgUERGIEZpbGUgMiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIC4uLmNvbnRpbnVlZCBmcm9tIHBhZ2UgMS4gWWV0IG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NzYuNjU2MCBUZA0KKCBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY2NC43MDQwIFRkDQooIHRleHQuIE9oLCBob3cgYm9yaW5nIHR5cGluZyB0aGlzIHN0dWZmLiBCdXQgbm90IGFzIGJvcmluZyBhcyB3YXRjaGluZyApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY1Mi43NTIwIFRkDQooIHBhaW50IGRyeS4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NDAuODAwMCBUZA0KKCBCb3JpbmcuICBNb3JlLCBhIGxpdHRsZSBtb3JlIHRleHQuIFRoZSBlbmQsIGFuZCBqdXN0IGFzIHdlbGwuICkgVGoNCkVUDQplbmRzdHJlYW0NCmVuZG9iag0KDQo4IDAgb2JqDQpbL1BERiAvVGV4dF0NCmVuZG9iag0KDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL05hbWUgL0YxDQovQmFzZUZvbnQgL0hlbHZldGljYQ0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCg0KMTAgMCBvYmoNCjw8DQovQ3JlYXRvciAoUmF2ZSBcKGh0dHA6Ly93d3cubmV2cm9uYS5jb20vcmF2ZVwpKQ0KL1Byb2R1Y2VyIChOZXZyb25hIERlc2lnbnMpDQovQ3JlYXRpb25EYXRlIChEOjIwMDYwMzAxMDcyODI2KQ0KPj4NCmVuZG9iag0KDQp4cmVmDQowIDExDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTkgMDAwMDAgbg0KMDAwMDAwMDA5MyAwMDAwMCBuDQowMDAwMDAwMTQ3IDAwMDAwIG4NCjAwMDAwMDAyMjIgMDAwMDAgbg0KMDAwMDAwMDM5MCAwMDAwMCBuDQowMDAwMDAxNTIyIDAwMDAwIG4NCjAwMDAwMDE2OTAgMDAwMDAgbg0KMDAwMDAwMjQyMyAwMDAwMCBuDQowMDAwMDAyNDU2IDAwMDAwIG4NCjAwMDAwMDI1NzQgMDAwMDAgbg0KDQp0cmFpbGVyDQo8PA0KL1NpemUgMTENCi9Sb290IDEgMCBSDQovSW5mbyAxMCAwIFINCj4+DQoNCnN0YXJ0eHJlZg0KMjcxNA0KJSVFT0YNCg==";

        /// <summary>
        /// Creación de sobre
        /// </summary>
        public static async Task Exectute()
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            // Definición de sobre
            var documentSet = new DocumentSet()
            {
                // Remitente del sobre
                SenderName = "FirmarOnline",
                // Mail del remitente
                SenderMail = "xxxxxxxxxx",
                // Nombre del sobre
                DocumentSetName = "ConsoleFirmarOnline",
                // Referencia externa de sobre para cliente
                //Reference = "reference",
                // Tipo de envío
                SendMethod = SendMethod.Email,
                // Número de días de recordatorio
                //ReminderDays = 1,
                // Número de días de validez del sobre
                ExpirationDaysTimeout = 3,

                // Firma corporativa
                //CorporateSignature = new CorporateSignature()
                //{ 
                //    Type = CorporateSignatureType.StartAndEnd,
                //    CorporateSignatureId = "xxxxxxxxxx";
                //},

                // Definición de documentos de sobre
                Documents = new List<Document>()
                {
                    // Documento 1
                    new Document()
                    {
                        Id = "Document1",
                        Name = "Document1Example",
                        B64PDFContent = pdfB64
                    },
                    // Documento 2
                    new Document()
                    {
                        Id = "Document2",
                        Name = "Document2Example",
                        B64PDFContent = pdfB64
                    }
                },

                // Definición de destinatarios de sobre
                Recipients = new List<Recipient>()
                {
                    // Destinatario 1
                    new Recipient()
                    {
                        // Identificador
                        Id = "Recipient1",
                        // Nombre
                        Name = "Recipient1 Name",
                        // Email
                        Email = "xxxxxxxxxx",
                        // Identificación (DNI, NIF, ...)
                        //CardId = "",
                        // Número de teléfono
                        //PhoneNumber = "+34...",

                        // Tipo de autenticación que debe realizar el destinatario sobre los documentos
                        AuthType = RecipientAuthenticationType.None,
                        // Para autenticación mediante código de acceso
                        //AccessCode = new RecipientAccessCode()
                        //{
                        //    Challenge = "Question",
                        //    Response = "ok"
                        //},

                        // Tipo de acción que debe realizar el destinatario sobre los documentos
                        ActionType = RecipientActionType.BioSignature,
                        
                        // Definición de las cajas de firma en cada uno de los documentos
                        Widgets = new List<RecipientAction>()
                        {
                            new RecipientAction()
                            {
                                // Identificador de documento
                                DocumentId = "Document1",

                                // Posicionamiento fijo
                                Widget = new FixedWidget()
                                {
                                    // Imagen de fondo de la caja de firma
                                    //B64Image = "",
                                    // Página
                                    Page = 1,
                                    // Desplazamiento horizontal desde la esquina inferior izquierda
                                    X = 200,
                                    // Desplazamiento vertical desde la esquina inferior izquierda
                                    Y = 100,
                                    // Texto personalizado a mostrar en la caja de firma
                                    CustomText = new List<TextLine>()
                                    {
                                        new TextLine()
                                        {
                                            Text = "Custom text"
                                        }
                                    },
                                    // Ancho
                                    Width = 200,
                                    // Alto
                                    Height = 100,
                                    // Rotación de la caja de firma
                                    Rotation = RotationType.Degrees_0,
                                }

                                // Posicionamiento flotante
                                //Widget = new FloatWidget()
                                //{
                                //     Imagen de fondo de la caja de firma
                                //    B64Image = "",
                                //     Texto personalizado a mostrar en la caja de firma
                                //    CustomText = new List<TextLine>()
                                //    {
                                //        new TextLine()
                                //        {
                                //            Text = "Custom text"
                                //        }
                                //    },
                                //     Rotación de la caja de firma
                                //    Rotation = 0,
                                //     Ancho
                                //    Width = 0,
                                //     Alto
                                //    Height = 0,
                                //     Texto a buscar
                                //    Text = "example",
                                //     Desplazamiento horizontal a partir del texto
                                //    GapX = 0,
                                //     Desplazamiento horizontal a partir del texto
                                //    GapY = 0
                                //}

                                //// Posicionamiento en un campo del PDF definido
                                //Widget = new FieldWidget()
                                //{
                                //    // Imagen de fondo de la caja de firma
                                //    B64Image = "",
                                //    // Texto personalizado a mostrar en la caja de firma
                                //    CustomText = new List<TextLine>()
                                //    {
                                //        new TextLine()
                                //        {
                                //            Text = "Custom text"
                                //        }
                                //    },
                                //    // Rotación de la caja de firma
                                //    Rotation = 0,
                                //    // Nombre del campo en el que se ubicará la caja de firma
                                //    FieldName = "fieldname"
                                //}
                            }
                        },

                        // Definición de anexos de sobre
                        Attachments = new List<RecipientDefinitionAttachment>()
                        {
                            new RecipientDefinitionAttachment()
                            {
                                // Descripción
                                Description = "Desription number one",
                                // Anexo requerido
                                Required = false
                            }
                        }
                    },
                    // Destinatario 2
                    new Recipient()
                    { 
                        Id = "Recipient2",
                        Name = "Recipient2 Name",
                        Email = "xxxxxxxxxx",
                        AuthType = RecipientAuthenticationType.Basic,
                        ActionType = RecipientActionType.AcceptanceSignature,
                        Widgets = new List<RecipientAction>()
                        {
                            new RecipientAction()
                            {
                                DocumentId = "Document1",
                                Widget = new FixedWidget()
                                {
                                    Page = 1,
                                    X = 300,
                                    Y = 100,
                                    Width = 100,
                                    Height = 50
                                }
                            },
                            new RecipientAction()
                            {
                                DocumentId = "Document2",
                                Widget = new FixedWidget()
                                {
                                    Page = 2,
                                    X = 200,
                                    Y = 300,
                                    Width = 200,
                                    Height = 100
                                }
                            },
                        }
                    }
                }
            };

            try
            {
                Console.WriteLine("Creando sobre...");

                // Llamada a API de FirmarOnline
                var id = await client.PostDocumentSetAsync(documentSet);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Sobre creado: " + id);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
