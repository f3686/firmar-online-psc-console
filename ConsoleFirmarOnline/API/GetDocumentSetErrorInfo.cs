﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using System.Text.Json;

namespace Api
{
    public class GetDocumentSetErrorInfo
    {
        /// <summary>
        /// Obtención de información de error de un sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obteniendo información error de sobre...");

                // Llamada a API de FirmarOnline
                var documentSetErrorInfo = await client.GetDocumentSetErrorInfoAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                // Formateo de salida a Json
                var jsonOptions = new JsonSerializerOptions { 
                    WriteIndented = true,
                    Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping
                };
                Console.WriteLine(JsonSerializer.Serialize(documentSetErrorInfo, jsonOptions));
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
