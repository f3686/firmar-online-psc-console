﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class GetEvidences
    {
        /// <summary>
        /// Obtención de evidencias del procesamiento de un sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obteniendo evidencias de sobre...");

                // Llamada a API de FirmarOnline
                var stream = await client.GetEvidencesAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Stream de evidencias obtenido. Longitud: " + stream.Length);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
