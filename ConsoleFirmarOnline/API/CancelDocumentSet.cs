﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class CancelDocumentSet
    {
        /// <summary>
        /// Cancelación de sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Cancelando sobre...");

                // Llamada a API de FirmarOnline
                await client.CancelDocumentSetAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Sobre cancelado");
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
