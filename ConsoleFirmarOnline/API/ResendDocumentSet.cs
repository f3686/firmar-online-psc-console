﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class ResendDocumentSet
    {
        /// <summary>
        /// Recordatorio al destinatario actual
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Reenviando email...");

                // Llamada a API de FirmarOnline
                await client.ResendDocumentSetAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Email reenviado");
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
