﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class GetDocument
    {
        /// <summary>
        /// Recuperación de un documento de sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        /// <param name="documentId">Identificador de documento</param>
        public static async Task Exectute(string documentSetId, string? documentId = null)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obteniendo documento de sobre...");

                // Llamada a API de FirmarOnline
                var stream = await client.GetDocumentAsync(documentSetId, documentId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Stream de documento obtenido. Longitud: "+stream.Length);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
