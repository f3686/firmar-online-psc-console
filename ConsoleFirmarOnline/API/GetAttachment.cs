﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class GetAttachment
    {
        /// <summary>
        /// Recuperación de un adjunto de sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        /// <param name="attachmentId">Identificador de adjunto</param>
        public static async Task Exectute(string documentSetId, string attachmentId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obteniendo adjunto de sobre...");

                // Llamada a API de FirmarOnline
                var fileResult = await client.GetAttachmentAsync(documentSetId, attachmentId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine("Nombre: " + fileResult.Name);
                Console.WriteLine("Content-Type: " + fileResult.ContentType);
                Console.WriteLine("Tamaño: " + fileResult.Content.Length);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
