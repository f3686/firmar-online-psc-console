﻿namespace Api
{ 
    public class Configuration
    {
        /// <summary>
        /// Url de API PSC
        /// </summary>
        public static string UrlPsc { get; } = "https://restapi.firmar.info/PSC/v40/";

        /// <summary>
        /// Url de API Verify
        /// </summary>
        public static string UrlVerify { get; } = "https://restapi.firmar.info/Verify/v40/";

        /// <summary>
        /// Token de acceso a API
        /// </summary>
        public static string Token { get; } = "";
    }
}
