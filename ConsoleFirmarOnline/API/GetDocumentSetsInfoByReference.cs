﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using System.Text.Json;

namespace Api
{
    public class GetDocumentSetsInfoByReference
    {
        /// <summary>
        /// Obtención de sobres coincidentes con un identificador externo
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string reference)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obtención de información de sobres por referencia...");

                // Llamada a API de FirmarOnline
                var collectionDocumentSetInfo = await client.GetDocumentSetsInfoByReferenceAsync(reference);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                // Formateo de salida a Json
                var jsonOptions = new JsonSerializerOptions { 
                    WriteIndented = true,
                    Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping
                };
                Console.WriteLine(JsonSerializer.Serialize(collectionDocumentSetInfo, jsonOptions));
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
