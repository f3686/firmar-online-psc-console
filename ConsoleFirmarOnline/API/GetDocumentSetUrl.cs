﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;

namespace Api
{
    public class GetDocumentSetUrl
    {
        /// <summary>
        /// Obtención de URL de acceso al sobre para los creados a través de NewDocumentSetUrl
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obteniendo url de acceso al sobre...");

                // Llamada a API de FirmarOnline
                var url = await client.GetDocumentSetUrlAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                Console.WriteLine(url);
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
