﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using System.Text.Json;

namespace Api
{
    public class GetDocumentSetStatus
    {
        /// <summary>
        /// Obtención de estado de sobre
        /// </summary>
        /// <param name="documentSetId">Identificador de sobre</param>
        public static async Task Exectute(string documentSetId)
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obtención de estado de sobre...");

                // Llamada a API de FirmarOnline
                var status = await client.GetDocumentSetStatusAsync(documentSetId);

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                // Formateo de salida a Json
                var jsonOptions = new JsonSerializerOptions { WriteIndented = true };
                Console.WriteLine(JsonSerializer.Serialize(status, jsonOptions));
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
