﻿using FirmarOnline.Clients.PSC;
using FirmarOnline.Model;
using System.Text.Json;

namespace Api
{
    public class GetHistory
    {
        /// <summary>
        /// Recuperación de listado de los sobres enviados a firmar
        /// </summary>
        public static async Task Exectute()
        {
            // Definición de cliente -> Url y Token de acceso al API
            var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);

            try
            {
                Console.WriteLine("Obtención de listado de sobres...");

                // Filtro de documentos
                //DocumentSetFilter documentSetFilter = new DocumentSetFilter()
                //{
                //    // Códigos de estado a mostrar
                //    Status = new DocumentSetStatusCode[] { DocumentSetStatusCode.Completed, DocumentSetStatusCode.Canceled },
                //    // Referencia externa de sobre para cliente
                //    Reference = "prueba1",
                //    // Número máximo de elementos a devolver
                //    Limit = 3,
                //    // Desplazamiento, número de elementos a saltarse
                //    Offset = 3,
                //    // Fecha/hora  de inicio
                //    FromDateTime = new DateTime(2022, 1, 1, 0, 0, 2),
                //    // Fecha/hora de fin
                //    ToDateTime = new DateTime(2024, 4, 6, 23, 59, 59)
                //};

                // Llamada a API de FirmarOnline
                var pageResult = await client.GetHistoryAsync(); // DocumentSetFilter filter *opcional

                // Salida con éxito
                Console.WriteLine("Response OK!!!!!!");
                // Formateo de salida a Json
                var jsonOptions = new JsonSerializerOptions { 
                    WriteIndented = true,
                    Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping
                };
                Console.WriteLine(JsonSerializer.Serialize(pageResult, jsonOptions));
            }
            catch (FirmarOnlineRequestException e)
            {
                // Escritura de excepción por consola
                e.WriteConsole();
            }
        }
    }
}
