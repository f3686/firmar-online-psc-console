﻿using Api;

// Variable de control
bool endConsole = false;

// Variables auxiliares de texto
string msgToken = "Se debe configurar el token de usuario";
string msgDocumentSetId = "Introduzca el identificador de sobre: ";
string msgFlowId = "Introduzca el identificador de flujo: ";
string msgDocumentId = "Introduzca el identificador de documento: ";
string msgAttachmentId = "Introduzca el identificador de adjunto: ";
string msgReference = "Introduzca una referencia de sobre: ";

// Variables auxiliares para lectura de datos
string documentSetId; // Identificador de sobre
string flowId; // Identificador de flujo
string documentId; // Identificador de documento
string attachmentId; // Identificador de anexo
string reference; // Referencia externa de cliente

Console.WriteLine("\n");
Console.WriteLine("Consola API conexión");
Console.WriteLine("--------------------\n");

// Validación de token
if (!ValidateToken())
{
    endConsole = true;
    Console.WriteLine(msgToken);
}

while (!endConsole)
{
    Console.WriteLine("Elija una operación a realizar:");
    Console.WriteLine("\n");
    Console.WriteLine("\t 1 - Creación de sobre con varios documentos y varios destinatarios");
    Console.WriteLine("\t 2 - Creación de sobre con un único documento y destinatario y devolución de url para acceder al visor de firma (NewDocumentSetUrl)");
    Console.WriteLine("\t 3 - Creación de sobre con un único documento y destinatario indicando el método de envío (Email o Sms)");
    Console.WriteLine("\t 4 - Creación de sobre a partir de un flujo con varios documentos y varios destinatarios, pudiendo informar valores");
    Console.WriteLine("\t 5 - Creación de sobre a partir de un flujo con varios documentos y varios destinatarios");
    Console.WriteLine("\t 6 - Creación de sobre a partir de un flujo con varios documentos y varios destinatarios, pudiendo informar valores y devolución de url para acceder al visor de firma (NewDocumentSetUrl)");
    Console.WriteLine("\t 7 - Cancelación de sobre");
    Console.WriteLine("\t 8 - Envío de recordatorio de sobre");
    Console.WriteLine("\t 9 - Estado de un sobre");
    Console.WriteLine("\t10 - Url de acceso a sobre creado a través de NewDocumentSetUrl");
    Console.WriteLine("\t11 - Información de error de un sobre");
    Console.WriteLine("\t12 - Información o detalle de un sobre");
    Console.WriteLine("\t13 - Recuperación de documento de sobre");
    Console.WriteLine("\t14 - Recuperación de documento de sobre con un único documento");
    Console.WriteLine("\t15 - Evidencias de sobre");
    Console.WriteLine("\t16 - Trazas de eventos generados en el procesamiento de sobre");
    Console.WriteLine("\t17 - Información de sobres coincidentes con un identificador externo");
    Console.WriteLine("\t18 - Legal Audit Trail (Json firmado(información del sobre + evidencias)) de sobre");
    Console.WriteLine("\t19 - Recuperación de adjunto de sobre");
    Console.WriteLine("\t20 - Listado de los sobres enviados a firmar");
    Console.WriteLine("\t21 - Verificación de certificado de trazabilidad");
    Console.WriteLine("\t22 - Verificación de firmas en documento PDF");
    Console.WriteLine("\n\tPresione 'e' y Enter para cerrar la aplicación\n");
    
    try
    {
        string? operation = string.Empty;

        while (string.IsNullOrEmpty(operation))
        {
            Console.Write("Elija opción: ");
            operation = Console.ReadLine();
        }

        switch (operation)
        {
            case "1":
                await PostDocumentSet.Exectute();
                break;
            case "2":
                await PostDocumentSetAndGetUrlAsync.Exectute();
                break;
            case "3":
                await PostDocumentSetSimple.Exectute();
                break;
            case "4":
                flowId = GetValue(msgFlowId);
                await PostDocumentSetFlow.Exectute(flowId);
                break;
            case "5":
                flowId = GetValue(msgFlowId);
                await PostDocumentSetFlowSimple.Exectute(flowId);
                break;
            case "6":
                flowId = GetValue(msgFlowId);
                await PostDocumentSetFlowAndGetUrl.Exectute(flowId);
                break;
            case "7":
                documentSetId = GetValue(msgDocumentSetId);
                await CancelDocumentSet.Exectute(documentSetId);
                break;
            case "8":
                documentSetId = GetValue(msgDocumentSetId);
                await ResendDocumentSet.Exectute(documentSetId);
                break;
            case "9":
                documentSetId = GetValue(msgDocumentSetId);
                await GetDocumentSetStatus.Exectute(documentSetId);
                break;
            case "10":
                documentSetId = GetValue(msgDocumentSetId);
                await GetDocumentSetUrl.Exectute(documentSetId);
                break;
            case "11":
                documentSetId = GetValue(msgDocumentSetId);
                await GetDocumentSetErrorInfo.Exectute(documentSetId);
                break;
            case "12":
                documentSetId = GetValue(msgDocumentSetId);
                await GetDocumentSetInfo.Exectute(documentSetId);
                break;
            case "13":
                documentSetId = GetValue(msgDocumentSetId);
                documentId = GetValue(msgDocumentId);
                await GetDocument.Exectute(documentSetId, documentId);
                break;
            case "14":
                documentSetId = GetValue(msgDocumentSetId);
                await GetDocument.Exectute(documentSetId);
                break;
            case "15":
                documentSetId = GetValue(msgDocumentSetId);
                await GetEvidences.Exectute(documentSetId);
                break;
            case "16":
                documentSetId = GetValue(msgDocumentSetId);
                await GetAuditTrail.Exectute(documentSetId);
                break;
            case "17":
                reference = GetValue(msgReference);
                await GetDocumentSetsInfoByReference.Exectute(reference);
                break;
            case "18":
                documentSetId = GetValue(msgDocumentSetId);
                await GetLegalAuditTrail.Exectute(documentSetId);
                break;
            case "19":
                documentSetId = GetValue(msgDocumentSetId);
                attachmentId = GetValue(msgAttachmentId);
                await GetAttachment.Exectute(documentSetId, attachmentId);
                break;
            case "20":
                await GetHistory.Exectute();
                break;
            case "21":
                await VerifyLegalAuditTrail.Exectute();
                break;
            case "22":
                await VerifySignedPDF.Exectute();
                break;
            case "e":
                endConsole = true;
                break;
            default:
                break;
        }
    }
    catch (Exception e)
    {
        Console.WriteLine("Ha ocurrido un error: " + e.Message);
    }
    Console.WriteLine("\n");
}

/// <summary>
/// Lectura de valor por teclado
/// </summary>
string GetValue(string message)
{
    string? value;

    Console.Write(message);
    value = Console.ReadLine();

    while (string.IsNullOrEmpty(value))
    {
        Console.Write("Entrada no válida. " + message);
        value = Console.ReadLine();
    }

    return value.Trim();
}

/// <summary>
/// Validación de token de configuración
/// </summary>
bool ValidateToken()
{
    return !string.IsNullOrEmpty(Configuration.Token);
}
