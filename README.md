# Firmar Online - Client PSC - Console

Programa de consola con ejemplos de llamadas y de integración con [firmar.online Public API](https://restapi.firmar.info).

# Compilación

Se puede compilar con Visual Studio 2022 o Visual Studio Code.

# Requerimiento previo

Estar dado de alta en la [plataforma de fima digital](https://app.firmar.info) de Edatalia.

![Interface](media/plataforma.jpg)

# Estructura del proyecto

**Program.cs**  
	Implementación de caso de uso, donde se presenta una interface con las funcionalidades disponibles en el API.

**Carpeta API**:  
	Contiene los diferentes archivos de configuración y ejemplos para realizar llamadas al API.

**Estrucutra de clase**:  
	Las diferentes clases de ejemplo de llamada a funcionalidades del API contienen el método ***Execute***, donde se muestra la configuración, la llamada y el procesamiento de la respuesta del API:
- Configuración de url y token del API.

   `var client = new PSCClient(new Uri(Configuration.UrlPsc), Configuration.Token);`

- Llamada a funcionalidad del API.

   `var id = await client.PostDocumentSetAsync(documentSet);`

   ** Preparación previa de parámetros de llamada si es necesario.

- Procesamiento de respuesta:  
Se muestra por consola el resultado obtenido en dependencia a la respuesta obtenida, si la respuesta es correcta normalmente se muestra en consola un Json de salida con la respuesta obtenida y si la respuesta es errónea se muestra en consola el contenido de la excepción ***FirmarOnlineRequestException*** recibida.

# Inicialización

Informar en la propiedad ***Token*** de la clase ***Configuration.cs*** la clave de usuario obtenida de la [plataforma de fima digital](https://app.firmar.info) de Edatalia para su correcto funcionamiento.

Para obtener una clave de usuario, ir al icono de usuario en la pantalla principal y seleccionar ***Integración API***.

![Interface](media/usuario.jpg)

***Generar una nueva clave de usuario*** y ***copiar y pegar*** en el proyecto de consola.

![Interface](media/integracion.jpg)

# Funcionamiento

Al ejecutar el programa de consola se presentan las diferentes funcionalidades del API:

![Interface](media/interface.jpg)

Seleccionar la funcionalidad a ejecutar y pulsar enter.
Para finalizar la demo y salir de la consola, introducir el caracter e y pulsar enter.

** La aplicación dará un error si no está configurado el token del usuario.

# Funcionalidades

1.  **Creación de sobre** con **varios documentos** y **varios destinatarios**:  
Crea un sobre de ejemplo con dos documentos y dos destinatarios, a través de la clase ***PostDocumentSet***.

2. **Creación de sobre** con **un único documento y destinatario** y **devolución de url** para acceder al visor de firma (*NewDocumentSetUrl*):  
Crea un sobre de ejemplo con un único documento y destinatario y devuelve la url de acceso al visor de firma, a través de la clase ***PostDocumentSetAndGetUrlAsync***.

3. **Creación de sobre** con **un único documento y destinatario** indicando el **método de envío** (Email o Sms):  
Crea un sobre de ejemplo con un único documento y destinatario, a través de la clase ***PostDocumentSetSimple***.

4. **Creación de sobre a partir de un flujo** con **varios documentos** y **varios destinatarios**, pudiendo informar valores de creación de sobre:  
Crea un sobre de ejemplo con dos documentos y dos destinatarios a partir un flujo (introducido por consola), pudiendo informar valores de creación de sobre, a través de la clase ***PostDocumentSetFlow***.

5. **Creación de sobre a partir de un flujo** con **varios documentos** y **varios destinatarios**:  
Crea un sobre de ejemplo con dos documentos y dos destinatarios a partir un flujo (introducido por consola), a través de la clase ***PostDocumentSetFlowSimple***.

6. **Creación de sobre a partir de un flujo** con **varios documentos** y **varios destinatarios**, pudiendo informar valores de creación de sobre  y **devolución de url** para acceder al visor de firma (*NewDocumentSetUrl*):  
Crea un sobre de ejemplo con dos documentos y dos destinatarios a partir de un flujo (introducido por consola), pudiendo informar valores de creación de sobre y devuelve la url de acceso al visor de firma, a través de la clase ***PostDocumentSetFlowAndGetUrl***.

7. **Cancelación de sobre**:  
Cancela un sobre (introducido por consola), a través de la clase ***CancelDocumentSet***.  
\*\* El sobre debe encontrarse en estado de *en proceso*.

8. **Envío de recordatorio de sobre**:  
Envía un recordatorio (email o sms) al destinatario actual o pendiente del proceso de un sobre (introducido por consola), a través de la clase ***ResendDocumentSet***.  
\*\* El sobre debe encontrarse en estado de *en proceso*.

9. **Estado de sobre**:  
Obtiene el estado de un sobre (introducido por consola), a través de la clase ***GetDocumentSetStatus***.

10. **Url de acceso a sobre** creado a través de *NewDocumentSetUrl*:  
Obtiene la url actual de acceso al visor de firma para un sobre (introducido por consola) creado a partir de *NewDocumentSet*, a través de la clase ***GetDocumentSetUrl***.  
\*\* El sobre debe encontrarse en estado de *en proceso*.

11. **Información de error de sobre**:  
Obtiene la información de error de un sobre (introducido por consola), a través de la clase ***GetDocumentSetErrorInfo***.  
\*\*  El sobre debe encontrarse en estado de *error*.

12. **Información o detalle de sobre**:  
Obtiene la información de un sobre (introducido por consola), a través de la clase ***GetDocumentSetInfo***.

13. **Recuperación de documento de sobre**:  
Recupera un documento (introducido por consola) de un sobre (introducido por consola), a través de la clase ***GetDocument***.  
\*\* El sobre debe encontrarse en estado *completado*.

14. **Recuperación de documento de sobre** con un único documento:  
Recupera el único documento de un sobre (introducido por consola), a través de la clase ***GetDocument***.  
\*\* El sobre debe encontrarse en estado *completado*.

15. **Evidencias de sobre**:  
Recupera el documento de evidencias del procesamiento de un sobre (introducido por consola), a través de la clase ***GetEvidences***.  
\*\* El sobre debe encontrarse en un estado final (*completado, cancelado, rechazado, expirado, en error*).

16. **Trazas de eventos generados** en el procesamiento del sobre:  
Obtiene las trazas de eventos generados en el procesamiento de un sobre (introducido por consola), a través de la clase ***GetAuditTrail***.

17. **Información de sobres** coincidentes con un **identificador externo**:  
Obtiene la información de los sobres coincidentes con un identificador externo (introducido por consola), a través de la clase ***GetDocumentSetsInfoByReference***.

18. **Legal Audit Trail de sobre**:  
Recupera el certificado de trazabilidad de un sobre (introducido por consola), que consiste en un Json firmado (información de sobre + evidencias) en formato JWT, a través de la clase ***GetLegalAuditTrail***.  
\*\* El sobre debe encontrarse en un estado final (*completado, cancelado, rechazado, expirado, en error*).

19. **Recuperación de adjunto de sobre**:  
Recupera un adjunto (introducido por consola) de un sobre (introducido por consola), a través de la clase ***GetAttachment***.  
\*\* El sobre debe encontrarse en estado *completado*.

20. **Listado de sobres** enviados a firmar:  
Obtiene un listado histórico de los sobres enviados a firmar, a través de la clase ***GetHistory***.

21. **Verificación de certificado de trazabilidad**:  
Verifica un certificado de trazabilidad de ejemplo, a través de la clase ***VerifyLegalAuditTrail***.

22. **Verificación de firmas en documento PDF**:  
Verifica las firmas que contiene un documento pdf firmado de ejemplo, a través de la clase ***VerifySignedPDF***.
